<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/bootstrap.bundle.min.js"></script>
    <link rel="icon" href="./img/icon.png">
    <title>Macbook Air M1 2020</title>
    <link rel="stylesheet" href="css/all.css">
</head>

<body style="background: black;">
    <header>
        <nav class="navbar navbar-expand-lg fixed-top"
            style="background: rgba(255, 255, 255, 0.1);border-bottom: 1px solid rgba(255, 255, 255, 0.2);backdrop-filter: blur(10px);">
            <div class="container">
                <a class="navbar-brand wht100 lt11 ps-3" id="navbarTitle" href="#" style="font-size: 26px;">Macbook
                    Air</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav">
                    <span class="bi bi-text-right wht70" style="font-size: 25px;"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav mt-4 mt-lg-0  ">
                        <li class="nav-item ps-3">
                            <a class="nav-link active" aria-current="page" href="#overview">Overview</a>
                        </li>
                        <li class="nav-item ps-3">
                            <a class="nav-link" href="#chip">Chip</a>
                        </li>
                        <li class="nav-item ps-3">
                            <a class="nav-link" href="#thermal">Thermal</a>
                        </li>
                        <li class="nav-item ps-3">
                            <a class="nav-link" href="#display">Display</a>
                        </li>
                        <li class="nav-item ps-3">
                            <a class="nav-link" href="#other">Other</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <br><br><br><br><br>
    <section class="d-flex justify-content-center">
        <img src="img/macbook.jpg" style="width: 100%;">
    </section>
    <br id="overview"><br><br><br><br>

    <main class="container">
        <section class="d-flex flex-column align-items-center">
            <p class="lt11 wht50" style="font-size: 48px; text-align: center;">Power. It’s in the</p>
            <p class="lt11 wht100" style="font-size: 89px;">&nbsp;Air.</p>
            <p class="lt8 wht100" style="width: clamp(300px, 65vw,1000px); font-size: 18px; text-align: center;">Our
                thinnest, lightest notebook,completely transformed by the Apple M1 chip. The longest battery life ever
                in a MacBook Air. And a silent, fanless design. This much power has never been this ready to go.</p>
            <br>
            <a href="https://www.apple.com/shop/buy-mac/macbook-air" target="blank" class="btn px-4 btnwht fs-6">From
                $999</a>
        </section>

        <br><br><br id="chip"><br><br>
        <br><br><br><br><br>

        <section class="row">
            <div class="col-12 col-md-6 d-flex flex-column align-items-center align-items-md-start">
                <img src="img/m1.png" width="120px" alt="">
                <p class="lt11 wht100 mb-2 text-center text-md-start" style="font-size: 40px;">Small
                    <span class="wht50">chip.</span> Giant
                    <span class="wht50">leap.</span>
                </p>
                <div class="lt8 wht100 text-center text-md-start" style="font-size: 18px;">
                    <p>First chip designed specifically for Mac.</p>
                    <p>Packed with an astonishing 16 billion transistors.</p>
                    <p>Integrates the CPU, GPU, Neural Engine, I/O, and so much more onto a single tiny chip.</p>
                </div>
            </div>
            <div class="col-12 col-md-6 align-self-center">
                <div class="d-flex justify-content-end">
                    <img src="img/chip.png" style="width: min(400px,100%);" alt="">
                </div>
            </div>
        </section>

        <br><br><br><br><br id="thermal">
        <br><br><br><br><br>

        <section class="row">
            <div class="col-12 col-md-6 col-lg-7 mb-4 m-md-0 align-self-center">
                <div class="d-flex justify-content-center">
                    <img src="img/thermal.jpg" style="border-radius: 20px;width: min(550px,95%);">
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5">
                <div class="d-flex justify-content-end text-center text-md-start">
                    <div>
                        <div class="lt8 wht50 m-0" style="font-size: 40px;">
                            <p class="m-0">No Fan.</p>
                            <p class="m-0">No Noise.</p>
                        </div>
                        <div class="lt8 wht100">
                            <p style="font-size: 60px;">Just Air.</p>
                            <p style="font-size: 18px;">With the industry-leading efficiency of the M1 chip, MacBook Air
                                delivers amazing performance without a fan.</p>
                            <p>The Aluminum heat spreader dissipates the heat, so no matter how intense the task,
                                MacBook Air runs completely silently.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <br><br><br><br><br>
        <br id="display"><br><br><br><br>

        <section class="row">
            <div class="d-flex flex-column align-items-center text-center">
                <p class="lt8 wht50" style="font-size: 40px;">Retina Display</p>
                <p class="lt8 wht70" style="font-size: 50px;">Lifelike Colors</p>
                <p class="lt8 wht100" style="font-size: 50px;">for unreal beauty.</p>
                <div>
                    <img src="img/display.png" style="width: min(890px, 100%)" alt="">
                </div>
            </div>
        </section>

        <br><br><br><br><br>

        <section class="row">
            <div class="col-12 col-lg-6 d-flex flex-column align-items-center pb-5 pb-lg-0">
                <div class="px-5 py-4 wht100"
                    style="width: min(500px,100%); background: linear-gradient(109.64deg, #4BBEC6 -37.36%, #011661 147.58%);border-radius: 40px;">
                    <p class="m-0 fs-2" style="filter: blur(1px);">Razor - Sharp text</p>
                    <p class="mb-4 fs-2 fs-xl-1" style="font-size: 40px; line-height: 20px;">Clarity.</p>
                    <p class="mb-0" style="font-size: 16px;">Extremely high pixel density makes text very clear.</p>
                </div>
                <div class="px-5 py-4 wht100 mt-5 mt-lg-4 mt-xl-5"
                    style="width: min(500px,100%); background: linear-gradient(109.64deg, #C6A34B -37.36%, #181408 147.58%);border-radius: 40px;">
                    <p class="m-0 fs-2 fs-xl-1">True Tone.</p>
                    <p class="fs-6">Good looking. And easy on the eyes.</p>
                    <p class="mb-0 fs-6">That automatically adjusts the white point of your
                        display
                        to the color temperature of your environment.</p>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex justify-content-center">
                <div class="p-5"
                    style="width: min(100%,552px); background:url(img/bg.png); background-repeat: no-repeat;background-size: contain; background-position: center;">
                    <div class="px-2 px-sm-5 px-lg-0 px-xl-5">
                        <p class="wht100 mb-0 lt8 fs-1 fs-sm-1">Wider</p>
                        <p class="wht100 lt8 mb-3 mb-sm-4 fs-4 fs-sm-2" style="line-height: 30px;">Pallete of Colors.
                        </p>
                        <p class="wht100 lt8 fs-2">P3 Color Gamut.</p>
                        <p class="wht100 lt8 mt-4 mt-sm-4 pt-0 pt-sm-5 pt-lg-3" style="font-size: min(4vw,20px);">13.3
                            inch Display. <br>
                            2560 x 1600 resolution.</p>
                    </div>
                </div>
            </div>
        </section>

        <br><br><br><br><br>
        <br><br><br id="other"><br><br>

        <section class="row p-4">
            <div class="col-12 col-md-5 col-xl-4 p-4">
                <div class="p-5 wht100"
                    style="border-radius: 40px; background: linear-gradient(152.71deg, #0B5184 7.53%, #6A0798 97.28%); height: 100%;">
                    <p class="fs-3 lt8 m-0">Faster</p>
                    <p class="fs-6 lt8" style="line-height: 5px;">Connection</p>
                    <div class="text-center pt-4 pb-5 d-flex align-items-center" style="height: 90%;">
                        <div class="w-100">
                            <p class="fs-1 lt8 m-0">&nbsp;Wi-Fi 6.</p>
                            <p class="fs-6 lt8">The newest generation of Wi-Fi .</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-7 col-xl-8 p-4">
                <div class="p-5 wht100"
                    style="border-radius: 40px; background: linear-gradient(243.39deg, #0088A6 -8.57%, #3400A3 111.38%); height: 100%;">
                    <p class="fs-4 lt8 m-0">Ultra High <span class="fs-6">Bandwidth &</span></p>
                    <p class="fs-3 lt8" style="line-height: 20px;">Versatility</p>
                    <div class="row pt-3">
                        <div class="col-12 col-xl-6 p-4 d-flex justify-content-start">
                            <img src="img/charge.png" alt="" style="padding: 0 7px 0 7px;">
                            <p class="ps-2 mb-0 fs-3 align-self-center lt8">Charging</p>
                        </div>
                        <div class="col-12 col-xl-6 p-4 d-flex justify-content-start">
                            <img src="img/monitor.png" alt="">
                            <p class="ps-2 mb-0 fs-5 align-self-center lt8" style="line-height: 20px;">
                                External<br>Display</p>
                        </div>
                        <div class="col-12 col-xl-6 p-4 d-flex justify-content-start">
                            <img src="img/transfer.png" alt="" style="padding: 0 11px 0 11px;">
                            <span>
                                <p class="ps-2 mb-0 fs-5 align-self-center lt8">Data Transfer</p>
                                <p class="ps-2 mb-0 fs-6 align-self-center lt8" style="line-height: 5px;">up to 40Gb/s
                                </p>
                            </span>
                        </div>
                        <div class="col-12 col-xl-6 p-4 d-flex justify-content-start">
                            <img src="img/device.png" alt="" style="padding: 0 15px 0 6px;">
                            <p class="ps-2 mb-0 fs-3 align-self-center lt8">Charging</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="row">
            <div class="col-12 col-md-6 px-5 py-3 py-md-0 pe-md-4">
                <div class="card w-100" style="border-radius: 40px; background: #1D1D1F;">
                    <div class="card-body py-5 text-center">
                        <h5 class="card-title fs-3 wht100">Explore Other Product.</h5>
                        <a href="https://www.apple.com/store" target="blank" class="btn btnwht fs-6 px-4">Shop</a>
                    </div>
                    <img src="img/product.png" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col-12 col-md-6 px-5 py-3 ps-md-4 py-md-0">
                <div class="card w-100" style="border-radius: 40px; background: #1D1D1F;height: 100%;">
                    <div class="card-body py-5 text-center">
                        <h5 class="card-title fs-3 wht100">Explore Mac Accessories.</h5>
                        <a href="https://www.apple.com/shop/mac/accessories" target="blank"
                            class="btn btnwht fs-6 px-4">Shop</a>
                    </div>
                    <img src="img/acc.png" class="card-img-top" alt="...">
                </div>
            </div>
        </section>

    </main>

    <br><br><br><br><br>

    <footer class="container text-center">
        <hr style="color: rgba(255, 255, 255, 1); height: 1px;">
        <p class="wht100 lt11">Copyright @ 2022 <a href="https://www.apple.com" class="wht100 lt11" target="blank">
                Apple Inc</a>.</p>
    </footer>
</body>

</html>